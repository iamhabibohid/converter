package com.mainor.distanceconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    public void milesToKm(View view){
        EditText distanceInput = (EditText) findViewById(R.id.distanceInput);
        TextView displayResult = (TextView) findViewById(R.id.displayResult);
        int input = Integer.parseInt(distanceInput.getText().toString());
        double r1 = input * 1.609344;
        String reults = Double.toString(r1);
        displayResult.setText(reults + " km");
    }

    public void kmToMiles(View view){
        EditText distanceInput = (EditText) findViewById(R.id.distanceInput);
        TextView displayResult = (TextView) findViewById(R.id.displayResult);

        int input = Integer.parseInt(distanceInput.getText().toString());
        double r1 = input * 0.62137;
        String reults = Double.toString(r1);
        displayResult.setText(reults + " miles");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
